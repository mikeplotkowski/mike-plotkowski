Mike Plotkowski is a San Francisco Realtor who relentlessly takes initiative to ensure his clients success. Buyers or sellers expectant of ultra-luxury concierge service would do well to consider his teams professionalism, integrity and confidentiality.

Website: https://mikeplotkowski.com/